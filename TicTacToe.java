import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.ArrayList;

public class TicTacToe extends JFrame{
    private Container pane;
    private String currentPlayer;
    private JButton [][] board;
    private boolean hasWinner;
    private int iter = 0;
    private JMenuBar menuBar;
    private JMenu menu;
    private JMenuItem quit;
    private JMenuItem newGame;
    private Point AImove;
    private int [][] aiboard = new int[3][3];
    private int PLAYER_X = 1;
    private int PLAYER_O = 2;

    public TicTacToe(){
        super();
        pane = getContentPane();
        pane.setLayout(new GridLayout(3,3));
        setTitle("TicTacToe");
        setSize(400, 400);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        currentPlayer = "o";
        board = new JButton[3][3];
        hasWinner = false;
        initializeBoard();
        intializeMenuBar();
    }

    private void intializeMenuBar(){
        menuBar = new JMenuBar();
        menu = new JMenu("File");
        newGame = new JMenuItem("New Game");
        quit = new JMenuItem("Quit");
        newGame.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                resetBoard();
            }
        });
        quit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.exit(0);
            }
        });
        menu.add(newGame);
        menu.add(quit);
        menuBar.add(menu);
        setJMenuBar(menuBar);
    }

    private void resetBoard(){
        currentPlayer = "o";
        hasWinner = false;
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                board[i][j].setText("");
                aiboard[i][j] = 0;
            }
        }
    }

    private void initializeBoard(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                JButton btn = new JButton();
                btn.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));
                board[i][j] = btn;
                btn.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        if (((JButton)e.getSource()).getText().equals("") && hasWinner == false){
                            btn.setText(currentPlayer);
                            
                            //togglePlayer();
                            getCurrentBoardState();
                        }
                    } 
                });
                pane.add(btn);
            }
        }
    }

    private void togglePlayer(){
        if (currentPlayer.equals("o")){
            currentPlayer = "x";
        }else{
            currentPlayer = "o";
        }
    }

    private void hasWinner(){
        //check if player won
        if((board[0][0].getText().equals(board[1][1].getText()) && board[0][0].getText().equals(board[2][2].getText()) && board[0][0].getText().equals(currentPlayer)) 
        || (board[0][2].getText().equals(board[1][1].getText()) && board[0][2].getText().equals(board[2][0].getText()) && board[0][2].getText().equals(currentPlayer))){
            JOptionPane.showMessageDialog(null, "Player " + currentPlayer + " has won!");
            hasWinner = true;
            resetBoard();
        }

        for(int i=0;i<3;i++){
            if((board[i][0].getText().equals(board[i][1].getText()) && board[i][0].getText().equals(board[i][2].getText()) && board[i][0].getText().equals(currentPlayer)) 
            || (board[0][i].getText().equals(board[1][i].getText()) && board[0][i].getText().equals(board[2][i].getText()) && board[0][i].getText().equals(currentPlayer))){
                JOptionPane.showMessageDialog(null, "Player " + currentPlayer + " has won!");
                hasWinner = true;
                resetBoard();
            }
        }

        if (getAvailCell().isEmpty()){
            JOptionPane.showMessageDialog(null, "It's a tie!");
            hasWinner = false;
            resetBoard();
        }
    }

    private void hasWinnerAI(){
        //check if AI won
        if((aiboard[0][0] == aiboard[1][1] && aiboard[0][0] == aiboard[2][2] && aiboard[0][0] == PLAYER_X) 
        || (aiboard[0][2] == aiboard[1][1] && aiboard[0][2] == aiboard[2][0]) && aiboard[0][2] == PLAYER_X){
            JOptionPane.showMessageDialog(null, "Player AI has won!");
            hasWinner = false;
            resetBoard();
        }

        for(int i=0;i<3;i++){
            if((aiboard[i][0] == aiboard[i][1] && aiboard[i][0] == aiboard[i][2] && aiboard[i][0] == PLAYER_X) 
            || (aiboard[0][i] == aiboard[1][i] && aiboard[0][i] == aiboard[2][i]) && aiboard[0][i] == PLAYER_X){
                JOptionPane.showMessageDialog(null, "Player AI has won!");
                hasWinner = false;
                resetBoard();
            }
        }

        if (getAvailCells().isEmpty()){
            JOptionPane.showMessageDialog(null, "It's a tie!");
            hasWinner = false;
            resetBoard();
        }
    }

    public boolean hasPlayerWon(int player){
        //check diagonal
        if((aiboard[0][0] == aiboard[1][1] && aiboard[0][0] == aiboard[2][2] && aiboard[0][0] == player) 
        || (aiboard[0][2] == aiboard[1][1] && aiboard[0][2] == aiboard[2][0]) && aiboard[0][2] == player){
            return true;
        }

        for(int i=0;i<3;i++){
            if((aiboard[i][0] == aiboard[i][1] && aiboard[i][0] == aiboard[i][2] && aiboard[i][0] == player) 
            || (aiboard[0][i] == aiboard[1][i] && aiboard[0][i] == aiboard[2][i]) && aiboard[0][i] == player){
                return true;
            }
        }
        return false;
    }

    private void getCurrentBoardState(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if (board[i][j].getText().equals("o")){
                    placeMove(new Point(i,j), 2);
                }
            }
        }
        
        minmax(0, 1);
        if(getAvailCells().size() == 9){
            //wait for player move
        }else{
            placeMove(AImove, 1);
            updateUI(AImove);
        }
    }

    private void updateUI(Point move){
        board[move.x][move.y].setText("x");
        //displayBoard();
        hasWinnerAI();
        hasWinner();
    }

    private List<Point> getAvailCell(){
        List<Point> availCells = new ArrayList<>();
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(board[i][j].getText().equals("")){
                    availCells.add(new Point(i,j));
                }
            }
        }
        return availCells;
    }

    private List<Point> getAvailCells(){
        List<Point> availCells = new ArrayList<>();
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(aiboard[i][j] == 0){
                    availCells.add(new Point(i,j));
                }
            }
        }
        return availCells;
    }

    private boolean placeMove(Point point, int player){
        if(aiboard[point.x][point.y] != 0){
            return false;
        }

        aiboard[point.x][point.y] = player;
        return true;
    }

    private int minmax(int depth, int turn){
        if(hasPlayerWon(PLAYER_X))
            return 1;
        if(hasPlayerWon(PLAYER_O))
            return -1;

        List<Point> availCells = getAvailCells();

        if(availCells.isEmpty())
            return 0;
        
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for(int i=0;i<getAvailCells().size();i++){
            Point point = availCells.get(i);

            if(turn == PLAYER_X){
                placeMove(point, PLAYER_X);
                int currentScore = minmax(depth + 1, PLAYER_O);
                max = Math.max(currentScore, max);

                if(currentScore >= 0){
                    if(depth == 0){
                        AImove = point;
                    }
                }

                if(currentScore == 1){
                    aiboard[point.x][point.y] = 0;
                    break;
                }

                if(i == availCells.size()-1 && max < 0){
                    if(depth == 0){
                        AImove = point;
                    }
                }
            }else if(turn == PLAYER_O){
                placeMove(point, PLAYER_O);
                int currentScore = minmax(depth + 1, PLAYER_X);
                min = Math.min(currentScore, min);

                if(min == -1){
                    aiboard[point.x][point.y] = 0;
                    break;
                }
            }
            
            
            aiboard[point.x][point.y] = 0; 
        }
        return turn == PLAYER_X ? max:min;
        
    }

    public void displayBoard(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(aiboard[i][j] + " ");
            }
            System.out.println();
        }
    }

}